const isBrowser = this.window === this;
if(!isBrowser){
  WebSocket = require('ws'); 
}

const cliWS = new WebSocket('ws://localhost:8080/');

cliWS.addEventListener('open', function sndRequest() {
  var obj = new Object();
  obj.a = 15;
  obj.b = 16
  cliWS.send(JSON.stringify(obj));
});
cliWS.addEventListener('message', function recResponse(data) {
  console.log(data.data);
  cliWS.close();
});