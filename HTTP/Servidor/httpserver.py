#!/usr/bin/env python
# -*- coding: utf-8 -*-

#Baseado no código disponibilizado em https://gist.github.com/bradmontgomery/2219997
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer

class S(BaseHTTPRequestHandler):
    list = set()
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        self._set_headers()
        response = "<html><body><h1>Oi JAI!</h1><ul>"
        for item in self.list:
                response += "<li>"+item+"</li>"
        response += "</ul></body></html>"
        self.wfile.write(response)

    def do_HEAD(self):
        self._set_headers()
    def do_DELETE(self):
        self._set_headers()
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        data = self.rfile.read(content_length)
        if (data not in self.list):
            self.wfile.write("<html><body><h3>O item \'"+data+"\' não existe</h3></body></html>")
        else:
            self.list.remove(data)
            self.wfile.write("<html><body><h3>O item \'"+data+"\' foi deletado</h3></body></html>")
        
    def do_PUT(self):
        content_length = int(self.headers['Content-Length']) # <--- Gets the size of data
        data = self.rfile.read(content_length)
        self._set_headers()
        if (data in self.list):
            self.wfile.write("<html><body><h3>O item \'"+data+"\' já existe</h3></body></html>")
        else:
            self.list.add(data)
            self.wfile.write("<html><body><h3>O item \'"+data+"\' foi adicionado</h3></body></html>")
        
def run(server_class=HTTPServer, handler_class=S, port=3000):
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd...'
    httpd.serve_forever()

if __name__ == "__main__":
    from sys import argv

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
