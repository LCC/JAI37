/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package coaptest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import org.eclipse.californium.core.CoapClient;
import org.eclipse.californium.core.CoapHandler;
import org.eclipse.californium.core.CoapResource;
import org.eclipse.californium.core.CoapResponse;
import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.network.CoapEndpoint;
import org.eclipse.californium.core.network.Endpoint;

import org.eclipse.californium.core.CoapServer;
import org.eclipse.californium.core.coap.CoAP;
import org.eclipse.californium.core.server.resources.CoapExchange;

import static org.eclipse.californium.core.coap.CoAP.ResponseCode.*;
import org.eclipse.californium.core.coap.Request;
import org.eclipse.californium.core.network.EndpointManager;
import org.eclipse.californium.core.network.config.NetworkConfig;
import org.eclipse.californium.core.server.resources.CoapExchange;

/**
 *
 * @author roberto.macedo
 */
public class CoAPTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        CoapEndpoint endpoint = new CoapEndpoint(63891);
        try 
        {
            endpoint.start();
        }catch(Exception ex){
            System.out.println("Erro. " + ex.getMessage());
        }
        //Syncronous
        
        CoapClient client = new CoapClient("coap://dominio:5683/ledVermelho");
        client.setEndpoint(endpoint);
        
        
        Request request = new Request(CoAP.Code.PUT);
        request.setPayload("0");
        request.setType(CoAP.Type.CON);
        
        CoapResponse response = client.advanced(request);
                       
        if (response != null) 
        {
            System.out.println( response.getCode() );
            System.out.println( response.getOptions() );
            System.out.println( response.getResponseText() );        	
        } 
        else 
        {	
            System.out.println("Request failed");        	
        }
        
        
        //Asyncronous   
        /*
        CoapClient client = new CoapClient("coap://192.168.43.82:5683/ligaled");
        client.setEndpoint(endpoint);
        
        Request request = new Request(CoAP.Code.PUT);
        request.setPayload("0");
        request.setType(CoAP.Type.CON);
        
        client.advanced(new CoapHandler() {
                        
			@Override 
                        public void onLoad(CoapResponse response) 
                        {
                                System.out.println("AQUI");
				String content = response.getResponseText();
				System.out.println("RESPONSE 3: " + content);
			}
			
			@Override 
                        public void onError() {
				System.err.println("FAILED");
			}
		},request);
        
        System.out.println("AQUI");
        
        // wait for user
	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	try 
        { 
            br.readLine(); 
        } 
        catch (IOException ex) 
        {  
        }
        */
    }
}
