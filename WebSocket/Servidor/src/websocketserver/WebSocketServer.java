package websocketserver;

import java.io.IOException;
import java.util.*;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.glassfish.tyrus.server.Server;

@ServerEndpoint(value = "/")
public class WebSocketServer {

    private static Set<Session> list = Collections.synchronizedSet(new HashSet<Session>());

    public static void main(String[] args) {
        new WebSocketServer().start();
    }

    @OnOpen
    public void onOpen(Session session) throws IOException {
        list.add(session);

    }

    @OnClose
    public void onClose(Session session) throws IOException {
        list.remove(session);

    }

    @OnMessage
    public void onMessage(String message, Session sessao) {
        System.out.println(message);
    }

    private void start() {
        Server server = new Server("0.0.0.0", 8080, "/", null, WebSocketServer.class);
        try {
            server.start();
            while (true) {
                Thread.sleep(1000);
                atualizaIntervaloClientes();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            server.stop();
        }
    }

    private void atualizaIntervaloClientes() throws IOException {
        for (Session s : list) {
            if (s.isOpen()) {
                s.getBasicRemote().sendText(getJson());
            }
        }
    }
    
    
    Random r = new Random();
    private String getJson() {
        String s = "{";
        s += "interval:";
        s += (r.nextInt(290) + 100);
        s += "}";
        return s;
    }
}
